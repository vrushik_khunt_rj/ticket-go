//
//  SubtaskCell.swift
//  Taskly
//
//  Created by mac on 17/03/22.
//

import UIKit

class SubtaskCell: UICollectionViewCell {
    @IBOutlet weak var lblSubtask: UILabel!
    @IBOutlet weak var viewOuter: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configureCell(_ title: String,isSelected: Bool){
        lblSubtask.text = title
        if isSelected{
            self.lblSubtask.textColor = .white
            self.viewOuter.backgroundColor = APP_PRIMARY_GREEN_COLOR
            self.viewOuter.layer.borderColor = UIColor.clear.cgColor
            self
        }else{
            self.lblSubtask.textColor = APP_GREY_TEXT_COLOR
            self.viewOuter.backgroundColor = .clear
            self.viewOuter.layer.borderColor = APP_GREY_TEXT_COLOR.cgColor

        }
    }

}
