//
//  TrackersCell.swift
//  Taskly
//
//  Created by mac on 17/03/22.
//

import UIKit

class TrackersCell: UITableViewCell {
    @IBOutlet weak var lblTotalTime: UILabel!
    @IBOutlet weak var viewTimeTimes: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCellWith(dict:[String:Any], isTrackerStarted:Bool){
        if isTrackerStarted{
            viewTimeTimes.isHidden = true
            lblTotalTime.textColor = APP_PRIMARY_GREEN_COLOR
        }else{
            viewTimeTimes.isHidden = false
            lblTotalTime.textColor = hexStringToUIColor(hex: "162C4E")
        }
    }
    
}
