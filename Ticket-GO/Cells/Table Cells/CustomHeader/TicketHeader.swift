//
//  TicketHeader.swift
//  Ticket-GO
//
//  Created by mac on 30/03/22.
//

import UIKit

class TicketHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var segmentControl: HBSegmentedControl!
    @IBOutlet weak var btnTap: UIButton!
  
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    override class func awakeFromNib() {
        super.awakeFromNib()
//        self.setupSegment(TicketHeader)
    }
    func setupSegment(){
        segmentControl.items = ["Today", "Week", "Month"]
        segmentControl.font = UIFont.init(name: FONT_Outfit_Medium, size: 12)
        segmentControl.unselectedLabelColor = hexStringToUIColor(hex: "717887")
        segmentControl.selectedLabelColor = .white
        segmentControl.backgroundColor = .clear
        segmentControl.thumbColor = hexStringToUIColor(hex: "6FD943")
        segmentControl.selectedIndex = 0

    }

    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
