//
//  InvoiceTableViewCell.swift
//  Taskly
//
//  Created by mac on 10/03/22.
//

import UIKit

class InvoiceTableViewCell: UITableViewCell {
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var lblInvoiceNumber: UILabel!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblInvoiceName: UILabel!
    @IBOutlet weak var lblCompanyname: UILabel!
    @IBOutlet weak var icCompanylogo: UIImageView!
    @IBOutlet weak var lblCurrencySymbol: UILabel!
    @IBOutlet weak var lblPriceAmount: UILabel!
    
    var currentIndex: Int!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(_ dict:[String:Any]){
        if currentIndex == 0{
            outerView.backgroundColor = hexStringToUIColor(hex: "FFEDE8")
            outerView.layer.borderColor = hexStringToUIColor(hex: "FF754C").cgColor
            viewStatus.backgroundColor = hexStringToUIColor(hex: "FF754C").withAlphaComponent(0.32)
            lblStatus.text = "Cancelled"
            lblStatus.textColor = hexStringToUIColor(hex: "FF754C")
            
        }else if currentIndex == 1{
            outerView.backgroundColor = .white
            outerView.layer.borderColor = hexStringToUIColor(hex: "DCE2ED").cgColor
            viewStatus.backgroundColor = hexStringToUIColor(hex: "FEEEB2")
            lblStatus.text = "Pending"
            lblStatus.textColor = hexStringToUIColor(hex: "FFC700")

        }else{
            outerView.backgroundColor = hexStringToUIColor(hex: "EDFFE5")
            outerView.layer.borderColor = hexStringToUIColor(hex: "6FD943").cgColor
            viewStatus.backgroundColor = hexStringToUIColor(hex: "6FD943").withAlphaComponent(0.20)
            lblStatus.text = "Paid"
            lblStatus.textColor = hexStringToUIColor(hex: "6FD943")

        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
