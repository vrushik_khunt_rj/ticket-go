//
//  CategoryCell.swift
//  Ticket-GO
//
//  Created by mac on 31/03/22.
//

import UIKit

class CategoryCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var viewCategory: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(_ category: CategoryItem){
        lblTitle.text = category.title
        lblCategory.text = category.title
        lblCategory.textColor = hexStringToUIColor(hex: category.color)
        viewCategory.layer.borderColor = hexStringToUIColor(hex: category.color).cgColor
    }
    
}
