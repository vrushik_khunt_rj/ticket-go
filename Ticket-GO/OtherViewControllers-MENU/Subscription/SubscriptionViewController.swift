//
//  SubscriptionViewController.swift
//  Taskly
//
//  Created by mac on 16/03/22.
//

import UIKit

class SubscriptionViewController: UIViewController {
    @IBOutlet weak var tablePaymentHistory: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tablePaymentHistory.register(UINib.init(nibName: "PaymentHistoryCell", bundle: nil), forCellReuseIdentifier: "PaymentHistoryCell")
        tablePaymentHistory.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    @IBAction func onClickClose(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }


}

extension SubscriptionViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentHistoryCell") as! PaymentHistoryCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
}
 
