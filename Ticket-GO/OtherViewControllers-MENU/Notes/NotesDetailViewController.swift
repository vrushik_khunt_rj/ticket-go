//
//  NotesDetailViewController.swift
//  Taskly
//
//  Created by mac on 14/03/22.
//

import UIKit

class NotesDetailViewController: UIViewController {
    @IBOutlet weak var viewAddAttachment: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewAddAttachment.addLineDashedStroke(pattern: [4,2], radius: 20, color: hexStringToUIColor(hex: "717887").cgColor)

        // Do any additional setup after loading the view.
    }
    @IBAction func onClickClose(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }

}
