//
//  NotesViewController.swift
//  Taskly
//
//  Created by mac on 14/03/22.
//

import UIKit

class NotesViewController: UIViewController {
    @IBOutlet weak var tablePersonalNotes: UITableView!
    @IBOutlet weak var tableSharedNotes: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        tablePersonalNotes.register(UINib.init(nibName: "NotesTableViewCell", bundle: nil), forCellReuseIdentifier: "NotesTableViewCell")
        tableSharedNotes.register(UINib.init(nibName: "NotesTableViewCell", bundle: nil), forCellReuseIdentifier: "NotesTableViewCell")
        tablePersonalNotes.tableFooterView = UIView()
        tableSharedNotes.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }

    @IBAction func onClickMenu(_ sender: Any){
//        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
//        vc.parentVC = self
//        vc.selectedItem = "Notes"
//        vc.view.backgroundColor = .clear
//        vc.modalPresentationStyle = .overCurrentContext
//        self.navigationController?.present(vc, animated: true)
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func onClickCreateNewNote(_ sender: Any){
        let vc = AddNewNoteVC(nibName: "AddNewNoteVC", bundle: nil)
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
        
    }


}

extension NotesViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tablePersonalNotes{
            return 2
        }
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: NotesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NotesTableViewCell") as! NotesTableViewCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        410
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.tablePersonalNotes{
            let vc = NotesDetailViewController(nibName: "NotesDetailViewController", bundle: nil)
            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(vc, animated: true)
        }else{
            let vc = NotesDetailViewController(nibName: "NotesDetailViewController", bundle: nil)
            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(vc, animated: true)
        }
    }
}
