//
//  FAQViewController.swift
//  Ticket-GO
//
//  Created by mac on 31/03/22.
//

import UIKit

class FAQViewController: UIViewController {
    @IBOutlet weak var tableFaq: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableFaq.register(UINib(nibName: "FaQCell", bundle: nil), forCellReuseIdentifier: "FaQCell")
        tableFaq.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickAddNewFaq(_ sender: Any){
        let vc = CreateFaqVC(nibName: "CreateFaqVC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion: nil)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension FAQViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FaQCell", for: indexPath) as! FaQCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        128
    }
}
