//
//  DashboardVC.swift
//  Taskly
//
//  Created by Vrushik on 08/03/22.
//

import UIKit
import JJFloatingActionButton

class DashboardVC: UIViewController, JJFloatingActionButtonDelegate {
    
    func floatingActionButtonWillOpen(_ button: JJFloatingActionButton) {
        actionButton.buttonColor = .white
        viewCircle.isHidden = false
    }
    func floatingActionButtonWillClose(_ button: JJFloatingActionButton) {
        actionButton.buttonColor = hexStringToUIColor(hex: "6FD943")
        viewCircle.isHidden = true
    }
    
    @IBOutlet weak var segmentTodays: HBSegmentedControl!
    @IBOutlet weak var segmentTask: HBSegmentedControl!
    @IBOutlet weak var viewCircle: UIView!
    @IBOutlet weak var tableTickets: UITableView!
    @IBOutlet weak var tableMoreTickets: UITableView!


    fileprivate let actionButton = JJFloatingActionButton()
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        return formatter
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        actionButton.buttonColor = hexStringToUIColor(hex: "6FD943")
        actionButton.delegate = self
        actionButton.buttonImageColor = hexStringToUIColor(hex: "172C4E")
        actionButton.overlayView.backgroundColor = .clear
        actionButton.buttonImageSize = CGSize(width: 16, height: 16)
        actionButton.itemAnimationConfiguration = .circularSlideIn(withRadius: 80)
        actionButton.buttonAnimationConfiguration = .rotation(toAngle: .pi * 3 / 4)
        actionButton.buttonAnimationConfiguration.opening.duration = 0.8
        actionButton.buttonAnimationConfiguration.closing.duration = 0.6
        for item in actionButton.items{
            item.imageView.tintColor = hexStringToUIColor(hex: "172C4E")
        }
        if #available(iOS 13.0, *) {
            actionButton.buttonImage = UIImage.init(named: "ic_plus")?.withTintColor(hexStringToUIColor(hex: "172C4E"))
        } else {
            // Fallback on earlier versions
        }
        let imgeTickets = UIImage.init(named: "tab_tickets")!.imageWithColor(color1: hexStringToUIColor(hex: "172C4E"))
        let imgageCateg = UIImage.init(named: "tab_categories")!.imageWithColor(color1: hexStringToUIColor(hex: "172C4E"))
        let imageUser = UIImage.init(named: "User")!.imageWithColor(color1: hexStringToUIColor(hex: "172C4E"))
        
        actionButton.addItem(image: imgeTickets) { item in
            let vc = CreateNewTicketVC(nibName: "CreateNewTicketVC", bundle: nil)
            vc.modalPresentationStyle = .overCurrentContext
            vc.view.backgroundColor = .clear
            self.navigationController?.present(vc, animated: true, completion: nil)
            
        }
        actionButton.addItem(image: imgageCateg) { item in
            //            Helper.showAlert(for: item)
            let vc = CreateNewCategoryVC(nibName: "CreateNewCategoryVC", bundle: nil)
            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(vc, animated: true, completion: nil)
            
        }
        
        actionButton.addItem(image: imageUser) { item in
            //            Helper.showAlert(for: item)
            let vc = InviteNewUserVC(nibName: "InviteNewUserVC", bundle: nil)
            vc.view.backgroundColor = .clear
            vc.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(vc, animated: true)
            
        }
        
        //        actionButton.display(inViewController: self)
        view.addSubview(actionButton)
        actionButton.translatesAutoresizingMaskIntoConstraints = false
        actionButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -18).isActive = true
        actionButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -36).isActive = true
        viewCircle.center = actionButton.center
        viewCircle.translatesAutoresizingMaskIntoConstraints = false

        

        //money
        segmentTodays.items = ["Today", "Week", "Month"]
        segmentTodays.font = UIFont.init(name: FONT_Outfit_Medium, size: 12)
        segmentTodays.unselectedLabelColor = hexStringToUIColor(hex: "717887")
        segmentTodays.selectedLabelColor = .white
        segmentTodays.backgroundColor = .clear
        segmentTodays.thumbColor = hexStringToUIColor(hex: "6FD943")
        segmentTodays.selectedIndex = 0

        //invoice
        segmentTask.items = ["Today", "Week", "Month"]
        segmentTask.font = UIFont.init(name: FONT_Outfit_Medium, size: 12)
        segmentTask.unselectedLabelColor = hexStringToUIColor(hex: "717887")
        segmentTask.selectedLabelColor = .white
        segmentTask.backgroundColor = .clear
        segmentTask.thumbColor = hexStringToUIColor(hex: "6FD943")
        segmentTask.selectedIndex = 0

        // Do any additional setup after loading the view.
        
        let nib = UINib(nibName: "TicketHeader", bundle: nil)
        let cellnib = UINib(nibName: "TicketsListCell", bundle: nil)
      
        tableTickets.register(nib, forHeaderFooterViewReuseIdentifier: "TicketHeader")
        tableTickets.register(cellnib, forCellReuseIdentifier: "TicketsListCell")
        tableTickets.tableFooterView = UIView()
        tableMoreTickets.register(UINib.init(nibName: "TicketMoreCell", bundle: nil), forCellReuseIdentifier: "TicketMoreCell")
        tableMoreTickets.tableFooterView = UIView()

    }
    
    @IBAction func onClickMenu(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.parentVC = self
        vc.selectedItem = "Dashboard"
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext

        self.navigationController?.present(vc, animated: true)
    }
    
    @IBAction func onClickNotifications(_ sender: Any){
        let vc = NotificationsViewController(nibName: "NotificationsViewController", bundle: nil)
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext

        self.navigationController?.present(vc, animated: true)

    }
    


}

extension DashboardVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableTickets{
            return 4
        }
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableTickets{
            let cell: TicketsListCell = tableView.dequeueReusableCell(withIdentifier: "TicketsListCell") as! TicketsListCell
            cell.selectionStyle = .none
            return cell
        }
        let cell: TicketMoreCell = tableView.dequeueReusableCell(withIdentifier: "TicketMoreCell") as! TicketMoreCell
        cell.selectionStyle = .none
        if indexPath.row <= 2{
            cell.backgroundColor = hexStringToUIColor(hex: "89E065").withAlphaComponent(0.08)
        }else{
            cell.backgroundColor = .clear
        }

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == self.tableTickets{
            return 117
        }
        return 60
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if tableView == self.tableTickets{
            
            let deleteAction = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
                // deleteAction code
                
            }
            deleteAction.image = UIImage.init(named: "ic_delete")
            deleteAction.title = "Delete"
            deleteAction.backgroundColor = hexStringToUIColor(hex: "FC275A")
            
            return UISwipeActionsConfiguration.init(actions: [deleteAction])
        }
        return nil
    }
}
