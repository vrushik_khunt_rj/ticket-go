//
//  TicketsVC.swift
//  Ticket-GO
//
//  Created by mac on 30/03/22.
//

import UIKit
import SwiftUI

class TicketsVC: UIViewController {
    @IBOutlet weak var tableTickets: UITableView!
    @IBOutlet weak var tableMoreTickets: UITableView!
    private var internalData : [WrapperObject]!


    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "TicketHeader", bundle: nil)
        let cellnib = UINib(nibName: "TicketsListCell", bundle: nil)
        internalData = [WrapperObject.init(header: HeaderObject.init(id: "In progress", isOpen: false), listObject: [ObjectDetail.init(id: "1", detailInfo: ""),
                                                                                                                     ObjectDetail.init(id: "1", detailInfo: ""),
                                                                                                                     ObjectDetail.init(id: "1", detailInfo: "")
                                                                                                                     ]),
                        WrapperObject.init(header: HeaderObject.init(id: "On Hold", isOpen: true), listObject: [ObjectDetail.init(id: "1", detailInfo: ""),
                                                                                                                                     ObjectDetail.init(id: "2", detailInfo: "")
                                                                                                                                     ]),
                        WrapperObject.init(header: HeaderObject.init(id: "Closed", isOpen: false), listObject: [ObjectDetail.init(id: "1", detailInfo: ""),
                                                                                                                                     ObjectDetail.init(id: "3", detailInfo: ""),
                                                                                                                                     ObjectDetail.init(id: "3", detailInfo: "")
                                                                                                                                     ,ObjectDetail.init(id: "3", detailInfo: ""),
                                                                                                                                     ObjectDetail.init(id: "3", detailInfo: ""),
                                                                                                                                     ObjectDetail.init(id: "3", detailInfo: "")])]
        
        tableTickets.register(nib, forHeaderFooterViewReuseIdentifier: "TicketHeader")
        tableTickets.register(cellnib, forCellReuseIdentifier: "TicketsListCell")
        tableTickets.tableFooterView = UIView()
        tableMoreTickets.register(UINib.init(nibName: "TicketMoreCell", bundle: nil), forCellReuseIdentifier: "TicketMoreCell")
        tableMoreTickets.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    @IBAction func onClickMenuBtn(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.parentVC = self
        vc.selectedItem = "Tickets"
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)
    }
   
    @IBAction func onClickNewTicket(_ sender: Any){
        let vc = CreateNewTicketVC(nibName: "CreateNewTicketVC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.view.backgroundColor = .clear
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    @objc func headerTapped(_ sender: UIButton){
        var header = self.internalData[sender.tag].header
        if header.isOpen{
            header.isOpen = false
        }else{
            header.isOpen = true
        }
        self.internalData[sender.tag].header = header
       
        UIView.animate(withDuration: 0.3) {
//            self.view.layoutIfNeeded()
            self.tableTickets.reloadData()

        }
    }
}

extension TicketsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = self.tableTickets.dequeueReusableHeaderFooterView(withIdentifier: "TicketHeader") as! TicketHeader
        view.setupSegment()
        view.btnTap.tag = section
        view.btnTap.addTarget(self, action: #selector(headerTapped(_:)), for: .touchUpInside)
        view.headerTitle.text = internalData[section].header.id
        return tableView == self.tableTickets ? view : UIView()

    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return tableView == self.tableTickets ? 60 : 0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView == self.tableTickets ? 117 : 60
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableView == self.tableTickets ? internalData.count : 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableTickets{
            if internalData[section].header.isOpen {
                return internalData[section].listObject.count
            } else {
                return 0
            }
            
        }else {
            return 5
        }

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableTickets{
            let cell: TicketsListCell = tableView.dequeueReusableCell(withIdentifier: "TicketsListCell") as! TicketsListCell
            cell.selectionStyle = .none
            return cell
        }else{
            let cell: TicketMoreCell = tableView.dequeueReusableCell(withIdentifier: "TicketMoreCell", for: indexPath) as! TicketMoreCell
            cell.selectionStyle = .none
            if indexPath.row <= 2{
                cell.backgroundColor = hexStringToUIColor(hex: "89E065").withAlphaComponent(0.08)
            }else{
                cell.backgroundColor = .clear

            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
