//
//  CreateNewTicketVC.swift
//  Ticket-GO
//
//  Created by mac on 30/03/22.
//

import UIKit

class CreateNewTicketVC: UIViewController {
    @IBOutlet weak var viewAttachments:UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewAttachments.addLineDashedStroke(pattern: [6,4], radius: 10, color: hexStringToUIColor(hex: "DFECFD").cgColor)

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickClose(_ sender: Any){
        self.dismiss(animated: true, completion: nil)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
