//
//  RecaptchaVC.swift
//  Ticket-GO
//
//  Created by mac on 01/04/22.
//

import UIKit

class RecaptchaVC: UIViewController {
    
    @IBOutlet weak var btnGoogle: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        btnGoogle.setImage(UIImage.init(named: "switch_on"), for: .selected)
        btnGoogle.setImage(UIImage.init(named: "switch_off"), for: .normal)

        // Do any additional setup after loading the view.
    }
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onClickGoogle(_ sender: Any){
        btnGoogle.isSelected = btnGoogle.isSelected == true ? false : true
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
