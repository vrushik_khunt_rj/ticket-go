//
//  TaxPlansViewController.swift
//  Taskly
//
//  Created by mac on 15/03/22.
//

import UIKit

class TaxPlansViewController: UIViewController {
    @IBOutlet weak var tableTaxPlanning: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableTaxPlanning.register(UINib.init(nibName: "TaxPlanningCell", bundle: nil), forCellReuseIdentifier: "TaxPlanningCell")
        tableTaxPlanning.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onClickCreatePlan(_ sender: Any){
//        let vc = AddTaxPlanVC(nibName: "AddTaxPlanVC", bundle: nil)
//        vc.view.backgroundColor = .clear
//        vc.modalPresentationStyle = .overCurrentContext
//        self.navigationController?.present(vc, animated: true, completion: nil)
    }



}
extension TaxPlansViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3

    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaxPlanningCell") as! TaxPlanningCell
        cell.selectionStyle = .none
        return cell

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
}
