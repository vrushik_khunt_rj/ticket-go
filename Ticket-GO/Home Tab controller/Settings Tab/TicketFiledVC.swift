//
//  TicketFiledVC.swift
//  Ticket-GO
//
//  Created by mac on 01/04/22.
//

import UIKit

class TicketFiledVC: UIViewController {
    @IBOutlet weak var tableTicket: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableTicket.register(UINib(nibName: "TicketFieldCell", bundle: nil), forCellReuseIdentifier: "TicketFieldCell")
        tableTicket.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }


}

extension TicketFiledVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TicketFieldCell", for: indexPath) as! TicketFieldCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
