//
//  SiteSettingsViewController.swift
//  Taskly
//
//  Created by mac on 15/03/22.
//

import UIKit

class SiteSettingsViewController: UIViewController {
    @IBOutlet weak var viewAddAttachment: UIView!
    @IBOutlet weak var viewLogo1: UIView!
    @IBOutlet weak var viewLogo2: UIView!
    @IBOutlet weak var btnLang: UIButton!
    @IBOutlet weak var btnGdpr: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewAddAttachment.addLineDashedStroke(pattern: [4,4], radius: 20, color: hexStringToUIColor(hex: "D1D1D1").cgColor)
        viewLogo1.addLineDashedStroke(pattern: [4,4], radius: 20, color: hexStringToUIColor(hex: "D1D1D1").cgColor)
        viewLogo2.addLineDashedStroke(pattern: [4,4], radius: 20, color: hexStringToUIColor(hex: "D1D1D1").cgColor)
        btnLang.setImage(UIImage.init(named: "switch_on"), for: .selected)
        btnLang.setImage(UIImage.init(named: "switch_off"), for: .normal)
        btnGdpr.setImage(UIImage.init(named: "switch_on"), for: .selected)
        btnGdpr.setImage(UIImage.init(named: "switch_off"), for: .normal)


        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickBack(_ sender: Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onCLickRTL(_ sender: Any){
        btnLang.isSelected = btnLang.isSelected == false ? true : false
    }
    @IBAction func onClickGDPR(_ sender:Any){
        btnGdpr.isSelected = btnGdpr.isSelected == false ? true : false

    }

}
