//
//  UsersVC.swift
//  Taskly
//
//  Created by Vrushik on 08/03/22.
//

import UIKit

class UsersVC: UIViewController {

    @IBOutlet weak var tableUsers: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableUsers.register(UINib.init(nibName: "UsersTableViewCell", bundle: nil), forCellReuseIdentifier: "UsersTableViewCell")
        tableUsers.tableFooterView = UIView()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickMenu(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.parentVC = self
        vc.selectedItem = "Users"
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)

    }
    @IBAction func onClickNewUser(_ sender: Any){
        let vc = InviteNewUserVC(nibName: "InviteNewUserVC", bundle: nil)
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)

    }

}

extension UsersVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UsersTableViewCell = tableView.dequeueReusableCell(withIdentifier: "UsersTableViewCell") as! UsersTableViewCell
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let changeAction = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
            // changeAction code
        }

        let deleteAction = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
            // deleteAction code
           
        }
        // Set the button's images
        changeAction.image = UIImage.init(named: "ic_adjust")
        changeAction.title = "Change"
        deleteAction.image = UIImage.init(named: "ic_delete")
        deleteAction.title = "Delete"
        changeAction.backgroundColor = hexStringToUIColor(hex: "162C4E")
        deleteAction.backgroundColor = hexStringToUIColor(hex: "FC275A")

        return UISwipeActionsConfiguration.init(actions: [deleteAction,changeAction])
    }
}
