//
//  CategoriesVC.swift
//  Ticket-GO
//
//  Created by mac on 31/03/22.
//

import UIKit

class CategoriesVC: UIViewController {
    @IBOutlet weak var tableCategories: UITableView!
    var arrCategories = [CategoryItem(title: "Questions", color: "009CFE"),
                         CategoryItem(title: "New Installation", color: "FE007A"),
                         CategoryItem(title: "Blog", color: "82919A"),
                         CategoryItem(title: "Support", color: "9611E7")]

    override func viewDidLoad() {
        super.viewDidLoad()

        tableCategories.register(UINib(nibName: "CategoryCell", bundle: nil), forCellReuseIdentifier: "CategoryCell")
        tableCategories.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    @IBAction func onClickMenu(_ sender: Any){
        let vc = MenuViewController(nibName: "MenuViewController", bundle: nil)
        vc.parentVC = self
        vc.selectedItem = "Invoices"
        vc.view.backgroundColor = .clear
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true)
    }

    @IBAction func onClickCreateCategory(_ sender: Any){
        let vc = CreateNewCategoryVC(nibName: "CreateNewCategoryVC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        self.navigationController?.present(vc, animated: true, completion:  nil)
    }
}

extension CategoriesVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCategories.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as! CategoryCell
        cell.selectionStyle = .none
        cell.configureCell(arrCategories[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let changeAction = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
            // changeAction code
        }

        let deleteAction = UIContextualAction.init(style: .normal, title: nil) { [weak self] (action, view, completion) in
            // deleteAction code
           
        }
        // Set the button's images
        changeAction.image = UIImage.init(named: "ic_adjust")
        changeAction.title = "Change"
        deleteAction.image = UIImage.init(named: "ic_delete")
        deleteAction.title = "Delete"
        changeAction.backgroundColor = hexStringToUIColor(hex: "162C4E")
        deleteAction.backgroundColor = hexStringToUIColor(hex: "FC275A")

        return UISwipeActionsConfiguration.init(actions: [deleteAction,changeAction])


    }
    
}
