//
//  MenuItem.swift
//  Taskly
//
//  Created by mac on 10/03/22.
//

import Foundation
import UIKit

class MenuItem: Codable{

    var title: String
    var image: String
    var isSelected: Bool
    
    init(title: String, image:String, isselected: Bool) {
        self.title = title
        self.image = image
        self.isSelected = isselected
    }

}

class SettingItem:Codable{
    var title: String
    var image: String
    init(title: String, image:String) {
        self.title = title
        self.image = image
    }
}
class CategoryItem: Codable
{
    var title: String
    var color: String
    init(title: String, color: String) {
        self.title = title
        self.color = color
    }

}

//Expandable Object
struct WrapperObject {
    var header : HeaderObject
    var listObject : [ObjectDetail]
}

struct HeaderObject {
    var id : String
    var isOpen : Bool
}

struct ObjectDetail {
    var id : String
    var detailInfo : String
}
